FROM geerlingguy/docker-centos7-ansible:latest
RUN yum update -y && yum install -y git python3-pip 
RUN pip3 install --proxy=http://proxy.ethz.ch:3128 -U pip && pip3 install --proxy=http://proxy.ethz.ch:3128 wheel setuptools_rust ansible
